/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;


import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.StartupConstants.ICON_TitleBar;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.LanguagePropertyType.SLIDESHOW_TITLE_PROMPT;
import static ssm.LanguagePropertyType.SUBMIT_BUTTON;
import static ssm.StartupConstants.UI_PROPERTIES_FILE_NAME;


/**
 *
 * @author Ali
 */
public class TitlePrompt {
    
    private String title;
    public  TitlePrompt(){
    
    
    Stage dialogBox = new Stage();
        //Setting title and custom icon
        dialogBox.setTitle("Set Title");
        //dialogBox.getIcons().add(new Image(getClass().getResourceAsStream("mainWindow.png")));
        
        //Formatting the inside of the stage
        VBox vBox = new VBox();
        vBox.setSpacing(10);
        vBox.setAlignment(Pos.CENTER);
        Scene scene = new Scene(vBox, 500, 200);
        
        
        Label label;
        Button submit;
        
        //Giving values to the label and buttons
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        label = new Label(props.getProperty(SLIDESHOW_TITLE_PROMPT.toString()));
        submit = new Button(props.getProperty(SUBMIT_BUTTON.toString()));
      
         //Creating a new textfield to get user input   
        TextField textField = new TextField();
        vBox.getChildren().addAll(label, textField);
        vBox.getChildren().addAll(submit);
        vBox.setSpacing(10);
        
        //Making the submit button functional
        submit.setOnAction(e -> {this.title = textField.getText();
                dialogBox.close();});
        
        dialogBox.setScene(scene);
        dialogBox.showAndWait();
        
        
        
}   //To retrieve title
    public String getTitle()
    {
        return title;
    }
    
    //To set title
    public void setTitle(String newTitle)
    {
        title = newTitle;
        
    }
    
    
}
