/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.layout.HBox;


import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import static ssm.StartupConstants.ICON_TitleBar;
import static ssm.StartupConstants.PATH_ICONS;



/**
 *
 * @author Ali
 */
public class LanguagePrompt {
    
    private String language;
    public  LanguagePrompt(){
    
    
    Stage dialog = new Stage();
        //dialog.initStyle(StageStyle.DECORATED);
        dialog.setTitle("Language Selection");
        //Image icon = new Image("mainWindow.png");
        //dialog.getIcons().add(icon);
        HBox hBox = new HBox();
        hBox.setSpacing(10);
        hBox.setAlignment(Pos.CENTER);
        Text text = new Text(10, 20, "Please Select Your Language");
        text.setFont(new Font(15));
        Scene scene = new Scene(hBox, 500, 200);
        Button english = new Button("English");
        Button urdu = new Button("اردو");
        english.setOnAction(e -> {this.language = "english";
        dialog.close();});
        urdu.setOnAction(e -> {this.language = "urdu";
        dialog.close();});
        hBox.getChildren().addAll(text);
        hBox.getChildren().addAll(english,urdu);
        dialog.setScene(scene);
        dialog.showAndWait();
        
        
        
}
    public String getLanguage()
    {
        return language;
    }
    
    
    
    
}
